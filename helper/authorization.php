<?php
session_start();
include '../helper/query.php';
$validator = new Validation();

class Login extends Db
{
    public function authorizeUser()
    {
        try {
            global $validator;
            if($validator->email_valid == true && $validator->password_valid == true) {
                $result = $this->connect()->query("SELECT * FROM `user_information` where email = '$validator->check_email'");
                $rowCount = $result->num_rows;
                if ($rowCount > 0) {
                    $get_result = $result->fetch_assoc();
                    $_SESSION['auth'] = $get_result['email'];
                    $_SESSION['auth_pwd'] = $get_result['password'];
                    $_SESSION['auth_id'] = $get_result['id'];
                    if ($validator->check_email == $_SESSION['auth']) {
                        if (password_verify($validator->check_password, $get_result['password'])) {
                            header('Location: homepage.php');
                        } else {
                            echo alert("Email and Password does not match.");
                        }
                    }
                } else {
                    echo alert("Invalid User");
                }
            }
        } catch(Exception $e ) {    
            echo "Connection Failed: ". $e->getMessage() . "<br >";
            exit();
        }
    }
}