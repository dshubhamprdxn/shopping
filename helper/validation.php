<?php
    include '../database/connect.php';
    // if (!isset($_SESSION['auth'])) { location(register); }
    $db = new Db();
    class Validation
    {
        public $first_name, $last_name, $email, $password, $confirm_password, $phone, $password_encryp, $address;
        public $err_first_name, $err_last_name, $err_email, $err_password, $err_confirm_password, $err_phone,$err_address;
        public $first_name_valid, $last_name_valid, $email_valid, $password_valid, $phone_valid, $confirm_password_valid, $address_valid;
        // first name validation
        public function fnameValidator($first_name)
        {
            $this->first_name = $first_name;
            if (empty($this->first_name)) {
                $this->err_first_name = "Please enter first name.";
            } elseif (!preg_match("/^[a-zA-Z]+((['. ][a-zA-Z ])?[a-zA-Z]*)*$/", $this->first_name)) {
                $this->err_first_name = "Invalid first name!";
            } else {
                $this->first_name_valid = true;
                return $this->first_name;
            }
        }

        // last name validation
        public function lnameValidator($last_name) 
        {
            $this->last_name = $last_name;
            if (empty($this->last_name)) {
                $this->err_last_name = "Please enter last name.";
            } elseif (!preg_match("/^[a-zA-Z]+((['. ][a-zA-Z ])?[a-zA-Z]*)*$/", $this->last_name)) {
                $this->err_last_name = "Invalid last name!";
            } else {
                $this->last_name_valid = true;
                return $this->last_name;
            }
        }

        // email validation
        public function emailValidator($email) {
            $this->email = $email;
            global $db;
            if (empty($this->email)) {
                $this->err_email = "Please enter email.";
            } elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->err_email = "Please enter valid email.";
            } else {
                $result = $db->connect()->query("SELECT * FROM `user_information` where email = '$this->email'");
                $rowCount = $result->num_rows;
                if ($rowCount > 0) {
                    $this->err_email = "Sorry entered email is already exist. Please enter another email.";
                } else {
                    $this->email_valid = true;
                    return $this->email;
                }
            }
        }
        
        // password validation
        public function passwordValidator($password) {
            $this->password = $password;
            if (empty($this->password)) {
                $this->err_password = "Password cannot be empty.";
            } elseif (!preg_match("/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/", $this->password)) {
                $this->err_password = "Password must be combination of letters, numbers, & special characters (!@#$%^&*).";
            } elseif (strlen($this->password) < 8) {
                $this->err_password = "Passwords must contain atleast 8 characters.";
            }  else {
                $this->password_valid = true;
                $this->password_encryp = password_hash($this->password, PASSWORD_DEFAULT);
                return $this->password_encryp;
            }
        }
        
        // re-type password validation
        public function confirmPasswordValidator($confirm_password) {
            $this->confirm_password = $confirm_password;
            if (empty($this->confirm_password)) {
                $this->err_confirm_password = "Please confirm your password.";
            } elseif ($this->confirm_password !== $this->password) {
                $this->err_confirm_password = "Password does not match.";
            } else {
                $this->confirm_password_valid = true;
            }
        }

        // phone validation
        public function phoneValidator($phone) {
            $this->phone = $phone;
            if (empty($this->phone)) {
                $this->err_phone = "Please enter phone number.";
            } elseif (strlen($this->phone) < 10) {
                $this->err_phone = "Please enter 10 digit phone number.";
            } elseif (!preg_match("/^[0-9]{10}+$/", $this->phone)) {
                $this->err_phone = "Please enter valid phone number.";
            } else {
                $this->phone_valid = true;
                return $this->phone;
            }
        }

        public function addressValidator($address) {
            $this->address = $address;
            if(empty($this->address)) {
                $this->err_address = "Please enter address.";
            } else {
                $this->address_valid = true;
                return $this->address;
            }
        }

        // email validations for login page
        public function emailAuthorize($email)
        {
            $this->email = $email;
            if (empty($this->email)) {
                $this->err_email = "Please enter email.";
            } elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->err_email = "Please enter valid email.";
            } else {
                $this->email_valid = true;
            }
        }

        // password validations for login page
        public function passwordAuthorize($password)
        {
            $this->password = $password;
            if (empty($this->password)) {
                $this->err_password = "Password cannot be empty.";
            } else {
                $this->password_valid = true;
            }
        }
    }
?>