<?php
session_start();
include 'validation.php';
include 'helper_functions.php'; 
$user_id = $_SESSION['auth_id'];
 class QueryDb extends Db
{
    public function insertData()
    {
        global $validator;
        if (isset($_POST['submit'])) {

            $sql = "INSERT INTO `user_information`(`first_name`, `last_name`, `email`, `password`, `phone`,`address`) VALUES ('$validator->first_name','$validator->last_name','$validator->email','$validator->password_encryp','$validator->phone','$validator->address')";
            $conn = $this->connect();
            (mysqli_query($conn, $sql)) ? header('Location:../views/login.php') : header('Location:../views/register.php');
            $validator->first_name = $validator->last_name = $validator->email = $validator->password = $validator->phone = $validator->address = "";
        }
    }

    public function displayProducts()
    {
        $sql = "SELECT * FROM `product_details`";
        $conn = $this->connect();
        $products = mysqli_query($conn ,$sql);
        while ($rows = mysqli_fetch_array($products)) {
            $product_list[] = $rows;
        }
        return $product_list;
    }

    public function addProducts()
    {
        // to get the product_id from url
        $product_id = $_GET['product_id'];
        $product_id = end(explode("=", $product_id));
        $user_id = $_SESSION['auth_id'];
        $check_db = "SELECT `id`, `product_id` FROM `user_product` WHERE id = '$user_id' and product_id = '$product_id'";
        $conn = $this->connect();
        $check_db = mysqli_query($conn, $check_db);
        $rowCount = $check_db->num_rows;
        if ($rowCount > 0) {
            echo alert("maximum 1 quantity for same product.");
        } else {
            $add_data = "INSERT INTO `user_product`(`id`, `product_id`) VALUES ('$user_id', '$product_id');";
            $conn = $this->connect();
            mysqli_query($conn, $add_data);
        }
    }

    public function displayCart()
    {
        $user_id = $_SESSION['auth_id'];
        $sql = "SELECT product_details.product_id,product_details.product_name,product_details.product_price 
        FROM user_product 
        INNER JOIN product_details ON user_product.product_id = product_details.product_id and user_product.id = '$user_id'
        GROUP BY product_details.product_id";
        $conn = $this->connect();
        $display_product = $conn->query($sql);
        $rowCount = $display_product->num_rows;
        if($rowCount > 0) {
            while ($rows = mysqli_fetch_array($display_product)) {
                $product_list[] = $rows;
            }
            return $product_list;
        } else {
            echo alert("your cart is empty");
        }
    }

    public function deleteUser()
    {
        $user_id = $_SESSION['auth_id'];
        $product_id = $_GET['product_id'];
        $delete = "DELETE FROM `user_product` WHERE id = '$user_id' and product_id = '$product_id'";
        $conn = $this->connect();
        mysqli_query($conn, $delete);
        header('location:cart.php');
    }

    public function totalCartValue()
    {
        $user_id = $_SESSION['auth_id'];
        $query = "SELECT product_details.product_price 
        FROM user_product 
        INNER JOIN product_details ON user_product.product_id = product_details.product_id and user_product.id = '$user_id'
        GROUP BY product_details.product_id";
        $conn = $this->connect();
        $query_run = $conn->query($query);
        $qty = 0;
        while ($rows = mysqli_fetch_array($query_run)) {
            $qty += $rows['product_price'];
        }
        return $qty;
    } 
    public function displayUser()
    {
        $user_id = $_SESSION['auth_id'];
        $sql = "SELECT * FROM `user_information` where id = $user_id";
        $conn = $this->connect();
        $user = mysqli_query($conn ,$sql);
        while ($rows = mysqli_fetch_array($user)) {
            $user_detail[] = $rows;
        }
        return $user_detail;
    }
}

$queryDb = new QueryDb();
