<?php
class Db
{
    private $host;
    private $username;
    private $password;
    private $db;    
    public $conn;

    public function __construct()
    {
        $this->connect();
        return $this->conn;
    }
    
    public function connect()
    {
        $this->host = 'localhost';
        $this->username = 'phpmyadmin';
        $this->password = 'root';
        $this->db = 'Shopping';
        try {
            $conn = new mysqli($this->host, $this->username, $this->password, $this->db);
            if($conn->connect_errno) {
                throw new Exception($conn->connect_error); 
            }
            return $conn;
        } catch(Exception $e ) {    
            echo "Connection Failed: ". $e->getMessage() . "<br >";
            exit();
        }
    }
}