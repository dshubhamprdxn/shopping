<?php 
    include '../helper/authorization.php';
    if (isset($_SESSION['auth'])) { header("location:homepage.php"); }
    if (isset($_POST['login'])) {
      $validator->email_valid = $validator->password_valid = false;
      $validator->err_email = $validator->err_password = "";
      $validator->check_email = $_POST['email_check'];
      $validator->check_password = $_POST['check_password'];
      $_SESSION['email'] = $_POST['email_check'];
      $validator->emailAuthorize($_POST['email_check']);
      $validator->passwordAuthorize($_POST['check_password']);
    }
    $logincheck = new Login();
    $logincheck->authorizeUser();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LOGIN PAGE</title>
		<link rel="stylesheet" media="screen" href="../assets/css/style.css" />
  </head>
  <body>
    <section class="main">
      <h2>LOGIN USER</h2>
      <div class="form-data">
        <form action="" method="post" class="contact">
            <div class="form-group">
                <label for="email_check">email :</label><span class="required">*</span>
                <input type="text" name="email_check" id="email_check" placeholder="Enter Email" value="<?php echo $validator->check_email; ?>">
                <span class="error"><?php echo $validator->err_email ?></span>
            </div>
            <div class="form-group">
                <label for="password_check">password :</label><span class="required">*</span>
                <input type="password" name="check_password" id="check_password" placeholder="Enter Password">
                <span class="error"><?php echo $validator->err_password ?></span>
            </div>
            <div class="form-group">
                <input type="submit" name="login" class="login submit" value="login">
                <span class="login">New user ? <a href="register.php">Register Here</a></span>
            </div>
        </form>
      </div>
    </section>
  </body>
</html>