<?php 
    session_start();
    if(isset($_SESSION['auth'])) {
        session_destroy();
        unset($_SESSION['auth']);
        header('Location: login.php');
    }
    session_destroy();
    unset($_SESSION['auth']);
    header('Location: login.php');
?>