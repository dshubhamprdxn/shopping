<?php
    include '../helper/query.php';
    if (isset($_SESSION['auth'])) { header("location:homepage.php"); }
    $validator = new Validation();
    if (isset($_POST['submit'])) {
        $validator->first_name_valid = $validator->last_name_valid = $validator->email_valid = $validator->password_valid = $validator->phone_valid = $validator->confirm_password_valid = $validator->address_valid = false;

        $first_name = $_POST['first-name'];
        $last_name = $_POST['last-name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];

        $validator->fnameValidator($first_name);
        $validator->lnameValidator($last_name);
        $validator->emailValidator($email);
        $validator->passwordValidator($password);
        $validator->confirmPasswordValidator($confirm_password);
        $validator->phoneValidator($phone);
        $validator->addressValidator($address);
        
        if ($validator->first_name_valid == true && $validator->last_name_valid == true && $validator->email_valid == true && $validator->password_valid == true && $validator->confirm_password_valid == true && $validator->phone_valid == true && $validator->address_valid == true) {
            $queryDb->insertData();
        }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>REGISTRATION PAGE</title>
		<link rel="stylesheet" media="screen" href="../assets/css/style.css" />
  </head>
  <body>
    <section class="main">
      <h2>REGISTER USER</h2>
      <div class="form-data">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="contact">
            <div class="form-group">
                <label for="first-name">first name :</label><span class="required">*</span>
                <input type="text" name="first-name" id="first-name" placeholder="Enter your First Name" value="<?php echo $validator->first_name; ?>">
                <span class="error"><?php echo $validator->err_first_name; ?></span>
            </div>
            <div class="form-group">
                <label for="last-name">last name :</label><span class="required">*</span>
                <input type="text" name="last-name" id="last-name" placeholder="Enter your Last Name" value="<?php echo $validator->last_name; ?>">
                <span class="error"><?php echo $validator->err_last_name; ?></span>
            </div>
            <div class="form-group">
                <label for="email">email :</label><span class="required">*</span>
                <input type="text" name="email" id="email" placeholder="Enter Email" value="<?php echo $validator->email; ?>">
                <span class="error"><?php echo $validator->err_email; ?></span>
            </div>
            <div class="form-group">
                <label for="password">password :</label><span class="required">*</span>
                <input type="password" name="password" id="password" placeholder="Enter Password">
                <span class="error"><?php echo $validator->err_password; ?></span>
            </div>
            <div class="form-group">
                <label for="confirm_password">re-type password :</label><span class="required">*</span>
                <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" >
                <span class="error"><?php echo $validator->err_confirm_password; ?></span>
            </div>
            <div class="form-group">
                <label for="phone">phone number :</label><span class="required">*</span>
                <input type="text" name="phone" id="phone" placeholder="Enter Phone Number" value="<?php echo $validator->phone; ?>">
                <span class="error"><?php echo $validator->err_phone; ?></span>
            </div>
            <div class="form-group">
              <label>Address :</label><span class="required">*</span>
              <textarea name="address" class="address" name="address"><?php echo $validator->address; ?></textarea>
              <span class="error"><?php echo $validator->err_address; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="submit" value="submit">
                <span class="login">Already a user ? <a href="login.php">Login</a></span>
            </div>
        </form>
      </div>
    </section>
  </body>
</html>