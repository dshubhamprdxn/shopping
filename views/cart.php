<?php 
include '../helper/query.php';
if (!isset($_SESSION['auth'])) { header("location:login.php"); }
$added_product = $queryDb->displayCart();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CART PAGE</title>
		<link rel="stylesheet" media="screen" href="../assets/css/style.css" />
  </head>
  <body>
    <section class="main">
      <h2>Cart Details</h2>
      <div class="logout edit">
        <a href="homepage.php" title="cart">Homepage</a>
      </div>
      <ul class="heading-data contact">
        <li><span>Product </span></li>
        <li><span>Product Name</span></li>
        <li><span>Product Price</span></li>
        <li><span>Delete</span></li>
      </ul>
      <div class= "cart-details">
        <?php foreach ($added_product as $row) { ?>
        <ul class ="user-details">
          <li>
            <figure>                    
              <img src="../assets/Images/2-home_default.jpg" alt="Hummingbird Printed T-shirt">
            </figure>
          <li>
          <li><?php echo $row['product_name']; ?></li>
          <li><?php echo $row['product_price']; ?></li>
          <li><a href="delete.php?product_id=<?php echo $row['product_id'] ?>">delete</a></li>
        </ul>
        <?php } ?>
      </div>
      <div class="payment-details">
        <div class="cart-value">
          Total Cart value : <?php echo $queryDb->totalCartValue();
          ?>
        </div>
        <div class="payment">
          <a href="payment.php" title="cart">Proceed To PayMent</a>
        </div>
      </div>
    </section>
  </body>
</html>