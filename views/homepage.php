<?php 
include '../helper/query.php';
if (!isset($_SESSION['auth'])) { header("location:login.php"); }
$product_list = $queryDb->displayProducts();
$data_list = $queryDb->addProducts();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HOME PAGE</title>
		<link rel="stylesheet" media="screen" href="../assets/css/style.css" />
  </head>
  <body>
    <section class="main">
    <div class="logout edit">
        <a href="cart.php" title="cart">Cart</a>
    </div>
    <div class="logout">
      <a href="logout.php">Logout</a>
    </div>
    <section class="products">
          <div class="wrapper">
            <h4 class="up center">popular products</h4>
            <ul class="popular-products center">
            <?php
            foreach ($product_list as $product) {
            ?>
              <li class="product-details">
                <figure>                    
                <img src="../assets/Images/2-home_default.jpg" alt="Hummingbird Printed T-shirt">
                </figure>
                <span class="product-name cap"><?php echo $product['product_name']; ?></span>
                <span class="product-price"><?php echo $product['Product_price']; ?></span>
                <div class="add-to-cart">
                  <a href="homepage.php?product_id=<?php echo $product['product_id'] ?>" title="Add to Cart">Add To Cart</a>
                </div>
              </li>
            <?php
            }
            ?>
            </ul>
          </div>
        </section>
    </section>
  </body>
</html>