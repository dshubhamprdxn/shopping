<?php 
include '../helper/query.php';
if (!isset($_SESSION['auth'])) { header("location:login.php"); }
$added_product = $queryDb->displayCart();
$user_details = $queryDb->displayUser();

if (isset($_POST['pay'])) {
  echo alert("payment successful.");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAYMENT</title>
		<link rel="stylesheet" media="screen" href="../assets/css/style.css" />
  </head>
  <body>
    <section class="main">
      <main>
        <div class="logout edit">
          <a href="homepage.php" title="cart">Homepage</a>
        </div>
        <div class="form-data"> 
          <h2>Make Payment</h2>
          <div >
            <h2>ShippedTo :</h2>
            <?php foreach ($user_details as $row) { ?>
              <h4>Name: <?php echo $row['first_name']." ".$row['last_name']; ?></h4>
              <h4>Email: <?php echo $row['email'] ?></h4>
              <h4>Phone: <?php echo $row['phone'] ?></h4>
              <h4>Address: <?php echo $row['address'] ?></h4>
            <?php } ?>
          </div>
          <h2>Payment : Cash On Delivery</h2>
          <form class="form-group" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> " method="post">
            <input type="submit" class="submit" name="pay" value="Pay <?php echo $queryDb->totalCartValue(); ?>">
          </form>
        </div>
      </main>
      </div>
    </section>
  </body>
</html>